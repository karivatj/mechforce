/* Mechforce
 *
 * @author Kari Vatjus-Anttila <karidaserious@gmail.com>
 *
 * For conditions of distribution and use, see copyright notice in LICENSE
 *
 * Preferences.h 1.00 by Kari Vatjus-Anttila
 *
 */

#ifndef PREFS_H
#define PREFS_H

int fullscreen_enabled;
int pref_fullscreen;
int pref_soundsoff;

#endif /*PREFS_H*/
